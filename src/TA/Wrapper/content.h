#ifndef CONTENT_H
#define CONTENT_H

using GETAIFUNC = void* (*)(void);

struct Content {
    GETAIFUNC getai;
    void* handle; // place for dlopen handle
};

#endif /* ifndef CONTENT_H */
