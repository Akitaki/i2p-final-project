# Makefile for the report document.
# This Makefile uses docker to generate PDF files.

DOCKERIMG = akitaki/pandoc

Report.pdf: Report.md
	pandoc $< -o $@ --pdf-engine=xelatex -V CJKmainfont="Noto Serif CJK TC"

docker: Report.md
	docker run --rm \
	    --user $(shell id -u):$(shell id -g) \
	    --volume $(PWD):/data/ \
	    $(DOCKERIMG) "make"

