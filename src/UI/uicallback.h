#ifndef UICALLBACK_H
#define UICALLBACK_H

#include "../TA/board.h"
#include "../TA/ultra_board.h"

#include <sstream>
#include <string>
#include <variant>

namespace ui {
struct LogBuilder {
    std::wstringstream wss{};

    inline LogBuilder() {}

    template <typename T> inline LogBuilder& operator<<(T&& rhs) {
        wss << rhs;
        return *this;
    }
};

/**
 * @brief Type that carries multiple callback messages to the UI
 */
struct CallbackEvent {
    struct Log {
        std::wstring str;
    };

    struct WcharInput {
        wchar_t ch;
    };

    struct ArrowPress {
        enum Value : uint8_t { Up, Down, Right, Left };
        inline ArrowPress(Value value) : value(value){};
        Value value;
    };

    struct TabPress {};

    struct StartGame {};

    struct Shutdown {};

    struct UpdateBoard {
        TA::UltraBoard board;
    };

    struct UserTurn {
        TA::BoardInterface::Tag tag;
    };

    struct ClearLog {};

    struct UserMove {
        int x, y;
    };

    static inline CallbackEvent newLog(LogBuilder& builder) {
        return {Log{builder.wss.str()}};
    }

    static inline CallbackEvent newWcharInput(wchar_t ch) {
        return {WcharInput{ch}};
    }

    static inline CallbackEvent newArrowPress(wchar_t ch) {
        switch (ch) {
        case L'A':
            return {ArrowPress(ArrowPress::Up)};
        case L'B':
            return {ArrowPress(ArrowPress::Down)};
        case L'C':
            return {ArrowPress(ArrowPress::Right)};
        case L'D':
            return {ArrowPress(ArrowPress::Left)};
        default:
            return {ArrowPress(ArrowPress::Up)};
        }
    }

    static inline CallbackEvent newShutdown() { return {Shutdown{}}; }

    static inline CallbackEvent newStartGame() { return {StartGame{}}; }

    static inline CallbackEvent newTabPress() { return {TabPress{}}; }

    static inline CallbackEvent newUpdateBoard(TA::UltraBoard board) {
        return {UpdateBoard{board}};
    }

    static inline CallbackEvent newUserTurn(TA::BoardInterface::Tag tag) {
        return {UserTurn{tag}};
    }

    static inline CallbackEvent newClearLog() { return {ClearLog{}}; }

    static inline CallbackEvent newUserMove(int x, int y) {
        return {UserMove{x, y}};
    }

    std::variant<
        Log,
        WcharInput,
        Shutdown,
        ArrowPress,
        StartGame,
        UpdateBoard,
        UserTurn,
        ClearLog,
        TabPress,
        UserMove>
        event;
};
} // namespace ui

#endif /* ifndef UICALLBACK_H */
