#ifndef MCTS_AI_H
#define MCTS_AI_H

#include "./support_types.h"

#include <TA/Wrapper/ai.h>
#include <TA/ultra_board.h>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <fstream>
#include <mutex>
#include <optional>
#include <thread>
#include <vector>

namespace pmcts {

/**
 * @brief Number of trees that are still being recycled. See
 * `pmcts::Tree::~Tree()` for more information.
 */
extern std::atomic_size_t deinitCount;

class PlayerAgent {
  public:
    PlayerAgent();

    static void propagateBack(Node* node, BoardStatus status);

    static void propagateIncomplete(Node* node);

    static Node* selectNode(Node* root);

    static void expand(pmcts::Node* node);

    static BoardStatus simulateRandomly(State initialState);

    std::pair<int, int> query();

    void updateOpponentMove(std::pair<int, int> move);

    void openLogFile(std::string path);

    void reset();

    void detachedClearSearchTree();

  private:
    /**
     * @brief Function intented to be used as the entry of worker threads.
     */
    void
    workerLoop(std::chrono::time_point<std::chrono::system_clock> startTime);

    std::optional<std::pair<int, int>> lastMove{};
    Board localBoard{};
    std::basic_ofstream<char> logfile{};

    size_t coreCount;
    std::atomic_size_t loopCount{0};

    Tree searchTree{};
};
} // namespace pmcts

class AI : public AIInterface {
  public:
    /**
     * @brief Get our side and store it in `selfIsO`
     */
    void init(bool order) override;

    /**
     * @brief Get enemy's move and store it in `mcts::lastMove`
     */
    void callbackReportEnemy(int x, int y) override;

    /**
     * @brief Query a move
     */
    std::pair<int, int> queryWhereToPut([[maybe_unused]] TA::UltraBoard ub) override;

  private:
    pmcts::PlayerAgent agent{};
};

#endif /* ifndef MCTS_AI_H */
