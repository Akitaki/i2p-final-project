#ifndef RAND_AI_H
#define RAND_AI_H

#include <TA/Wrapper/ai.h>
#include <TA/ultra_board.h>

#include <fstream>
#include <optional>
#include <random>

template <typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template <typename Iter> Iter select_randomly(Iter start, Iter end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}

std::optional<std::pair<int, int>> lastMove;
std::ofstream logfile;

class AI : public AIInterface {
    bool selfIsO;

  public:
    void init(bool order) override {
        selfIsO = order;
        logfile.open((order) ? ("log-o.txt") : ("log-x.txt"));
        // any way
    }

    void callbackReportEnemy(int x, int y) override {
        lastMove = std::make_pair(x, y);
    }

    std::pair<int, int> queryWhereToPut(TA::UltraBoard ub) override {
        if (lastMove.has_value()) {
            logfile << "Has last move\n";
            auto [x, y] = lastMove.value();
            auto subX   = x % 3;
            auto subY   = y % 3;

            std::vector<std::pair<int, int>> possibleMoves;

            for (auto x = 0; x < 3; x++) {
                for (auto y = 0; y < 3; y++) {
                    auto tag = ub.get(subX * 3 + x, subY * 3 + y);
                    if (tag == TA::BoardInterface::Tag::None) {
                        possibleMoves.emplace_back(subX * 3 + x, subY * 3 + y);
                    }
                }
            }

            if (possibleMoves.size() == 0) {
                for (auto x = 0; x < 9; x++) {
                    for (auto y = 0; y < 9; y++) {
                        auto tag = ub.get(x, y);
                        if (tag == TA::BoardInterface::Tag::None) {
                            possibleMoves.emplace_back(x, y);
                        }
                    }
                }
            }

            return *select_randomly(possibleMoves.begin(), possibleMoves.end());
        } else {
            logfile << "No last move\n";
            std::vector<std::pair<int, int>> possibleMoves;

            if (possibleMoves.size() == 0) {
                for (auto x = 0; x < 9; x++) {
                    for (auto y = 0; y < 9; y++) {
                        auto tag = ub.get(x, y);
                        if (tag == TA::BoardInterface::Tag::None) {
                            possibleMoves.emplace_back(x, y);
                        }
                    }
                }
            }

            return *select_randomly(possibleMoves.begin(), possibleMoves.end());
        }
    }

  private:
    ~AI() { logfile.close(); }
};

#endif /* ifndef RAND_AI_H */
