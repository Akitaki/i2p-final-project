#include <sstream>

namespace ui {
/** @brief The temporary buffer for UI output */
extern std::wstringstream stdoutwss;
} // namespace ui
