#ifndef GAME_H
#define GAME_H

#include "../Misc/channel.h"
#include "../TA/Wrapper/ai.h"
#include "../TA/ultra_board.h"
#include "../UI/uicallback.h"

#include <algorithm>
#include <fstream>
#include <future>
#include <optional>

namespace game {

using TA::Board;
using TA::BoardInterface;
using TA::UltraBoard;

// Forward declare for LogAppender
class Game;

/**
 * @brief Utility struct to make logging syntax cleaner
 */
struct LogAppender {
    ui::LogBuilder builder;
    Game& game;

    inline LogAppender(Game& game) : builder(), game(game) {}

    ~LogAppender();

    template <typename T> inline LogAppender& operator<<(T&& rhs) {
        builder << rhs;
        return *this;
    }
};

/**
 * @brief Game runner implementation
 */
class Game final {
  public:
    /**
     * @brief Control message object. It's used to send movements to game
     * threads.
     */
    struct CtrlMsg {
        struct UserMove {
            int x, y;
        };

        struct QuitGame {};

        inline static CtrlMsg newUserMove(int x, int y) {
            return {UserMove{x, y}};
        }

        inline static CtrlMsg newQuitGame() { return {QuitGame{}}; }

        std::variant<UserMove, QuitGame> event;
    };

    /**
     * @brief Construct an instance of `Game` from a callback sender and
     * optional runtime limit.
     *
     * @param uiCallbackSender The callback sender for sending game events
     * (player moves, log messages etc) back to the UI.
     * @param runtimeLimit The runtime limit on shared library functions.
     * Default value is 1's.
     */
    Game(
        util::Sender<ui::CallbackEvent> uiCallbackSender,
        std::chrono::milliseconds runtimeLimit =
            std::chrono::milliseconds(1000));

    /** @brief Set the underlying player agent to `ptr` */
    void setPlayer1(AIInterface* ptr);

    /** @brief Set the underlying player agent to `ptr` */
    void setPlayer2(AIInterface* ptr);

    /**
     * @brief Run a game and return the winner back. Upon unrecoverable
     * errors or forced quit, it will return `std::nullopt` instead.
     */
    std::optional<BoardInterface::Tag> run();

    /**
     * @brief Get the sender of control channel for sending control messages to
     * game threads.
     * */
    inline util::Sender<CtrlMsg>& sender() { return ctrlChannel.first; }

    friend LogAppender;

  private:
    enum class PlayResult { Success, Fail, Quit };

    inline LogAppender log() { return LogAppender(*this); }

    /**
     * @brief Send the current main board to UI thread for updating.
     */
    inline void sendUpdateUI() {
        uiCallbackSender.send(ui::CallbackEvent::newUpdateBoard(mainBoard));
    }

    /**
     * @brief Take the opposite side of the tag (i.e. O to X and X to O).
     * Nothing will be done to `None`.
     */
    static BoardInterface::Tag oppose(BoardInterface::Tag tag);

    /**
     * Run the game one round and return true on success, otherwise (errors
     * etc.) false.
     */
    PlayResult playOneRound(
        std::optional<AIInterface*> user,
        BoardInterface::Tag tag,
        std::optional<AIInterface*> enemy);

    /**
     * @brief Update the main board winner status
     */
    void updateMainBoardWinTag();

    /**
     * Return true if the game is over or false otherwise.
     */
    bool checkGameover();

    /**
     * @brief Call `AIInterface::init()` on both player agents
     */
    bool prepareState();

    template <
        typename Func,
        typename... Args,
        std::enable_if_t<
            std::is_void<
                std::invoke_result_t<Func, AIInterface, Args...>>::value,
            int> = 0>
    inline void call(Func func, AIInterface* ptr, Args... args) {
        std::future_status status;
        auto val = std::async(std::launch::async, func, ptr, args...);
        status   = val.wait_for(std::chrono::milliseconds(runtimeLimit));

        if (status != std::future_status::ready) {
            throw std::runtime_error("Agent failed to return in time limit");
        }
        val.get();
    }

    template <
        typename Func,
        typename... Args,
        std::enable_if_t<
            std::is_void<std::invoke_result_t<Func, AIInterface, Args...>>::
                    value == false,
            int> = 0>
    inline auto call(Func func, AIInterface* ptr, Args... args)
        -> std::invoke_result_t<Func, AIInterface, Args...> {
        std::future_status status;
        auto val = std::async(std::launch::async, func, ptr, args...);
        status   = val.wait_for(std::chrono::milliseconds(runtimeLimit));

        if (status != std::future_status::ready) {
            throw std::runtime_error("Agent failed to return in time limit");
        }
        return val.get();
    }

    /** @brief Check if the ABI version of a shared library is matching */
    inline static bool checkAI(AIInterface* ptr) {
        return ptr->abi() == AI_ABI_VER;
    }

    /** @brief Get the receiver of the control channel. */
    inline util::Receiver<CtrlMsg>& receiver() { return ctrlChannel.second; }

    /* Members */

    /** @brief The runtime limit, measured in ms */
    std::chrono::milliseconds runtimeLimit;

    /** @brief First player agent */
    std::optional<AIInterface*> playerAgent1 = std::nullopt;
    /** @brief Second player agent */
    std::optional<AIInterface*> playerAgent2 = std::nullopt;

    /** @brief The game board */
    UltraBoard mainBoard;

    /**
     * @brief Last legal move done by the players
     */
    std::optional<std::pair<int, int>> lastMove = std::nullopt;

    /**
     * @brief Winner status array for the 9 small 3x3 subboards
     */
    std::array<BoardInterface::Tag, 9> subBoardStatusList{{
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
        BoardInterface::Tag::None,
    }};

    /**
     * @brief The debug log file stream
     */
    std::ofstream logfile;

    /** @brief Channel for receiving control events */
    std::pair<util::Sender<CtrlMsg>, util::Receiver<CtrlMsg>> ctrlChannel;

    /** @brief Channel sender to send events to UI */
    util::Sender<ui::CallbackEvent> uiCallbackSender;
};
} // namespace game

#endif /* ifndef GAME_H */
