#include "./game.h"

#include "TA/Wrapper/ai.h"
#include "UI/uicallback.h"

#include <iostream>
#include <variant>

// Forward declare
namespace ui {
extern wchar_t tagToWchar(TA::BoardInterface::Tag tag);
}

// Functions for debug purpose
namespace {
/**
 * @brief Dump a `TA::UltraBoard` as `std::string`
 */
std::string dumpBoard(TA::UltraBoard& ub) {
    std::stringstream ss;

    ss << "---------\n";
    // For each column
    for (auto x = 0; x < 9; x++) {
        // For each element
        for (auto y = 0; y < 9; y++) {
            switch (ub.get(x, y)) {
            case TA::BoardInterface::Tag::O:
                ss << 'O';
                break;
            case TA::BoardInterface::Tag::X:
                ss << 'X';
                break;
            case TA::BoardInterface::Tag::None:
                ss << ' ';
                break;
            default:
                ss << '?';
            }
        }
        ss << '\n';
    }
    ss << "---------\n";

    return ss.str();
}

/**
 * @brief Convert a tag to `const char*` for printing.
 */
const char* tagToStr(TA::BoardInterface::Tag tag) {
    switch (tag) {
    case TA::BoardInterface::Tag::O:
        return "O";
    case TA::BoardInterface::Tag::X:
        return "X";
    case TA::BoardInterface::Tag::Tie:
        return "=";
    case TA::BoardInterface::Tag::None:
        return "-";
    default:
        throw std::runtime_error("Unknown BoardInterface::Tag variant");
    }
}

/**
 * @brief Convert a status list to `std::string` for printing.
 */
std::string
dumpStatus(const std::array<TA::BoardInterface::Tag, 9>& statusList) {
    std::stringstream ss;

    ss << "--stat---";
    for (auto i = 0; i < 9; i++) {
        if (i % 3 == 0)
            ss << '\n';
        ss << tagToStr(statusList[i]);
    }
    ss << "\n---------\n";

    return ss.str();
}
} // namespace

namespace game {
LogAppender::~LogAppender() {
    game.uiCallbackSender.send(ui::CallbackEvent::newLog(builder));
}

/* Game member functions */

Game::Game(
    util::Sender<ui::CallbackEvent> uiCallbackSender,
    std::chrono::milliseconds runtimeLimit)
    : runtimeLimit(runtimeLimit), mainBoard(),
      ctrlChannel(util::createChannel<CtrlMsg>()),
      uiCallbackSender(uiCallbackSender) {
    logfile.open("game.log");
}

void Game::setPlayer1(AIInterface* ptr) {
    if (checkAI(ptr) != true)
        throw std::runtime_error("AI check failed");
    playerAgent1 = ptr;
}

void Game::setPlayer2(AIInterface* ptr) {
    if (checkAI(ptr) != true)
        throw std::runtime_error("AI check failed");
    playerAgent2 = ptr;
}

std::optional<BoardInterface::Tag> Game::run() {
    if (!prepareState())
        return std::nullopt;

    // Initial UI preparing
    sendUpdateUI();

    // The tag for current round
    BoardInterface::Tag tag = BoardInterface::Tag::O;

    // Copy the pointers to player agents. This allows us to simply
    // `std::swap()` on them to switch players on round changes.
    std::optional<AIInterface*> first  = playerAgent1;
    std::optional<AIInterface*> second = playerAgent2;

    // Setup initial win tag
    updateMainBoardWinTag();

    // Main game loop
    while (!checkGameover()) {
        const auto peekMsg = receiver().tryPeek();
        if (peekMsg.has_value() &&
            std::holds_alternative<CtrlMsg::QuitGame>(peekMsg->event)) {
            std::wcout << "Received quit control message\n";
            // We're told to force quit
            return std::nullopt;
        }

        const auto playResult = playOneRound(first, tag, second);

        // `playResult` indicates whether the play was successful.
        if (playResult == PlayResult::Success) {
            // Legal move is done by AIs.

            updateMainBoardWinTag();

            // We now update the UI, and then set tag to the opposite side.
            sendUpdateUI();
            // Set current player tag to the opposite
            tag = oppose(tag);

            // Swap player agents
            std::swap(first, second);
        } else if (playResult == PlayResult::Fail) {
            // Illegal operations are done by AIs.
            // We now terminate the game.
            // tui->warn() << L"Illegal move done by " <<
            // ui::tagToWchar(tag);
            break;
        } else if (playResult == PlayResult::Quit) {
            // Received quit control message
            log() << L"Force quitting game";
            return std::nullopt;
        }

        // Flush the debug log file
        logfile.flush();
    }

    sendUpdateUI();

    // Now a winner should be present as the tag in mainBoard.
    switch (mainBoard.getWinTag()) {
    case BoardInterface::Tag::O:
        log() << L"Game over. Winner: O";
        return BoardInterface::Tag::O;
    case BoardInterface::Tag::X:
        log() << L"Game over. Winner: X";
        return BoardInterface::Tag::X;
    case BoardInterface::Tag::Tie:
        log() << L"Game over (Tie)";
        return BoardInterface::Tag::Tie;
    case BoardInterface::Tag::None:
        log() << L"No winner. Bug?";
        return std::nullopt;
    default:
        throw std::runtime_error("Unknown BoardInterface::Tag variant");
    }
}

BoardInterface::Tag Game::oppose(BoardInterface::Tag tag) {
    if (tag == BoardInterface::Tag::O)
        return BoardInterface::Tag::X;
    if (tag == BoardInterface::Tag::X)
        return BoardInterface::Tag::O;
    return BoardInterface::Tag::None;
}

Game::PlayResult Game::playOneRound(
    std::optional<AIInterface*> user,
    BoardInterface::Tag tag,
    std::optional<AIInterface*> enemy) {

    int x = 0, y = 0;
    try {
        if (user.has_value()) {
            // Case I: It's an AI
            std::tie(x, y) =
                call(&AIInterface::queryWhereToPut, user.value(), mainBoard);
        } else {
            // Case I: It's human
            uiCallbackSender.send(ui::CallbackEvent::newUserTurn(tag));

            auto event = receiver().recv();

            const bool shouldQuit = std::visit(
                [&](auto&& event) {
                    using T = std::decay_t<decltype(event)>;
                    if constexpr (std::is_same_v<T, CtrlMsg::UserMove>) {
                        std::tie(x, y) = std::make_pair(event.x, event.y);
                        return false;
                    } else if constexpr (std::is_same_v<T, CtrlMsg::QuitGame>) {
                        return true;
                    } else {
                        logfile << "Unknown control message";
                        logfile.flush();
                        return false;
                    }
                },
                event.event);

            if (shouldQuit == true) {
                return PlayResult::Quit;
            }
        }
    } catch (std::exception& e) {
        logfile << "Exception during player move\n";
        logfile << e.what();
        logfile.close();

        // Exception during `call()`, set winner to opponent
        mainBoard.setWinTag(oppose(tag));
        return PlayResult::Fail;
    }

    // Start verifying if this is a legal move
    if (lastMove.has_value()) {
        // (x, y) of last move by opponent
        auto [lx, ly] = lastMove.value();

        // (s, t) is the position in its "small board"
        const auto ls = lx % 3;
        const auto lt = ly % 3;

        const bool isHoriWrong = ls != x / 3;
        const bool isVertWrong = lt != y / 3;

        constexpr std::array<std::pair<int, int>, 9> subBoardCells{{
            {0, 0},
            {0, 1},
            {0, 2},
            {1, 0},
            {1, 1},
            {1, 2},
            {2, 0},
            {2, 1},
            {2, 2},
        }};

        const bool isSubBoardFull = !std::any_of(
            subBoardCells.begin(), subBoardCells.end(), [&](auto p) {
                return mainBoard.get(p.first + ls * 3, p.second + lt * 3) ==
                       BoardInterface::Tag::None;
            });

        if ((isHoriWrong || isVertWrong) && (!isSubBoardFull)) {
            // Illegal move, set winner to opponent
            mainBoard.setWinTag(oppose(tag));

            log() << L"Illegal move: (" << x << ", " << y << L")";

            // Debug print
            {
                logfile << "Illegal move (Wrong board)\n";
                logfile << "Move by "
                        << ((tag == BoardInterface::Tag::O) ? ("O") : ("X"))
                        << '\n';

                logfile << "Move at (" << x << ", " << y << ")\n";
            }
            return PlayResult::Fail;
        }
    }

    if (mainBoard.get(x, y) != BoardInterface::Tag::None) {
        // Illegal move (the cell is already occupied), set winner to
        // opponent
        log() << L"Illegal move: (" << x << ", " << y << L")";

        // Debug print
        {
            logfile << "Illegal move (occupied)\n";
            logfile << "Move by "
                    << ((tag == BoardInterface::Tag::O) ? ("O") : ("X"))
                    << '\n';

            logfile << "Move at (" << x << ", " << y << ")\n";
        }
        mainBoard.setWinTag(oppose(tag));
        return PlayResult::Fail;
    }

    // Update board
    mainBoard.get(x, y) = tag;

    // Print UI message
    log() << ui::tagToWchar(tag) << L" placed at (" << x << L", " << y << L")";

    // Debug print
    {
        logfile << "Move by "
                << ((tag == BoardInterface::Tag::O) ? ("O") : ("X")) << '\n';

        logfile << "Move at (" << x << ", " << y << ")\n";

        // Dump board to log file
        logfile << dumpBoard(mainBoard);
    }

    // Report to opponent
    try {
        if (enemy.has_value()) {
            call(&AIInterface::callbackReportEnemy, *enemy, x, y);
        }
    } catch (std::exception& e) {
        logfile << "callbackReportEnemy raised an exception:\n";
        logfile << e.what();
    }

    // update last move
    lastMove = {x, y};

    return PlayResult::Success;
}

void Game::updateMainBoardWinTag() {
    /*
     * Part I: Update the individual subboards
     */

    // List of all winning configurations
    static constexpr std::array<std::array<std::pair<size_t, size_t>, 3>, 8>
        winConfList{
            {{{{0, 0}, {0, 1}, {0, 2}}},
             {{{1, 0}, {1, 1}, {1, 2}}},
             {{{2, 0}, {2, 1}, {2, 2}}},
             {{{0, 0}, {1, 0}, {2, 0}}},
             {{{0, 1}, {1, 1}, {2, 1}}},
             {{{0, 2}, {1, 2}, {2, 2}}},
             {{{0, 0}, {1, 1}, {2, 2}}},
             {{{0, 2}, {1, 1}, {2, 0}}}}};

    // Update subboard status list
    for (int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; y++) {
            // Get reference to the status for this subboard
            BoardInterface::Tag& status = subBoardStatusList[y * 3 + x];

            // Don't update those subboard status that are already generated
            if (status != BoardInterface::Tag::None) {
                continue;
            }

            // Get reference to the subboard
            Board& subBoard = mainBoard.sub(x, y);

            // Flag whether status is already generated.
            // It's false until we found somebody winning.
            bool updatedStatus = false;

            for (auto&& winConf : winConfList) {
                // Whether O wins
                const bool isOWin =
                    std::all_of(winConf.begin(), winConf.end(), [&](auto&& p) {
                        return subBoard.get(p.first, p.second) ==
                               BoardInterface::Tag::O;
                    });

                // Whether X wins
                const bool isXWin =
                    std::all_of(winConf.begin(), winConf.end(), [&](auto&& p) {
                        return subBoard.get(p.first, p.second) ==
                               BoardInterface::Tag::X;
                    });

                if (isOWin) {
                    // Set status to O and break
                    status = BoardInterface::Tag::O;
                    subBoard.setWinTag(status);
                    updatedStatus = true;
                    break;
                }

                if (isXWin) {
                    // Set status to X and break
                    status = BoardInterface::Tag::X;
                    subBoard.setWinTag(status);
                    updatedStatus = true;
                    break;
                }
            }

            // If this status is already updated above, continue
            if (updatedStatus) {
                continue;
            }

            // For each cell inside this subboard
            for (size_t x = 0; x < 3; x++) {
                for (size_t y = 0; y < 3; y++) {
                    // If (x, y) in this subboard is `None`, then its
                    // status can't be decided yet
                    if (subBoard.get(x, y) == BoardInterface::Tag::None) {
                        status = BoardInterface::Tag::None;
                        subBoard.setWinTag(status);
                        updatedStatus = true;
                        break;
                    }
                }
                if (updatedStatus)
                    break;
            }

            // If this status is already updated above, continue
            if (updatedStatus) {
                continue;
            }

            // If all checks above aren't matched, then it's a tie
            status = BoardInterface::Tag::Tie;
            subBoard.setWinTag(status);
        }
    }

    /*
     * Part II: Update the whole game board
     *
     * This part is similar to Part I, except for that we're now testing for
     * the whole board. Comments are therefore ommited (please refer to
     * comments in Part I).
     */

    // Whether the status for the whole board is updated
    bool updatedStatus = false;

    for (auto&& winConf : winConfList) {
        const bool isOWin =
            std::all_of(winConf.begin(), winConf.end(), [&](auto&& p) {
                return subBoardStatusList[p.second * 3 + p.first] ==
                       BoardInterface::Tag::O;
            });
        const bool isXWin =
            std::all_of(winConf.begin(), winConf.end(), [&](auto&& p) {
                return subBoardStatusList[p.second * 3 + p.first] ==
                       BoardInterface::Tag::X;
            });

        if (isOWin) {
            mainBoard.setWinTag(BoardInterface::Tag::O);
            updatedStatus = true;
        }

        if (isXWin) {
            mainBoard.setWinTag(BoardInterface::Tag::X);
            updatedStatus = true;
        }
    }

    if (updatedStatus) {
        return;
    }

    for (size_t x = 0; x < 3; x++) {
        for (size_t y = 0; y < 3; y++) {
            if (subBoardStatusList[y * 3 + x] == BoardInterface::Tag::None) {
                mainBoard.setWinTag(BoardInterface::Tag::None);
                return;
            }
        }
    }

    mainBoard.setWinTag(BoardInterface::Tag::Tie);
}

bool Game::checkGameover() {
    logfile << dumpStatus(subBoardStatusList);

    const auto winTag = mainBoard.getWinTag();
    if (winTag != BoardInterface::Tag::None) {
        return true;
    }

    return false;
}

bool Game::prepareState() {
    if (playerAgent1.has_value())
        call(&AIInterface::init, *playerAgent1, true);

    if (playerAgent2.has_value())
        call(&AIInterface::init, *playerAgent2, false);

    return true;
}

} // namespace game
