#include "./mcts_ai.h"
#include "./support_types.h"

#include <chrono>
#include <algorithm>

namespace mcts {

std::atomic_size_t deinitCount{0};

double uct(double ttlVisit, double winScore, double visitCount) {
    if (visitCount == 0)
        return std::numeric_limits<double>::max();

    return (winScore / visitCount / 4) +
           1.414 * std::sqrt(log(ttlVisit) / visitCount);
}

Node* PlayerAgent::selectNode(Node* root) {
    if (root->children.size() == 0) {
        return root;
    }

    Node* node = root;

    while (node->children.size() != 0) {
        const auto parentVisit = node->state.visitCount;
        node                   = *std::max_element(
            node->children.begin(),
            node->children.end(),
            [&](Node* lhs, Node* rhs) {
                const auto lhs_uct = uct(
                    parentVisit, lhs->state.winScore, lhs->state.visitCount);
                const auto rhs_uct = uct(
                    parentVisit, rhs->state.winScore, rhs->state.visitCount);
                return lhs_uct < rhs_uct;
            });
    }

    return node;
}

void PlayerAgent::expand(mcts::Node* node) {
    const auto derivedStates = node->state.getDerivedStates();
    for (const auto& state : derivedStates) {
        node->children.emplace_back(new Node{state, node});
    }
}

BoardStatus PlayerAgent::simulateRandomly(Node* node) {
    auto tmpNode  = Node{*node};
    auto tmpState = node->state;
    auto status   = tmpState.board.getStatus();

    if (status == BoardStatus::Lose) {
        if (tmpNode.parent.has_value()) {
            auto parent            = tmpNode.parent.value();
            parent->state.winScore = std::numeric_limits<int64_t>::min();
            return status;
        } else {
            return status;
        }
    }

    while (status == BoardStatus::Wip) {
        tmpState.side = negate(tmpState.side);
        tmpState.playRandomly();
        status = tmpState.board.getStatus();
    }

    return status;
}

void PlayerAgent::propagateBack(Node* node, BoardStatus status) {
    std::optional<Node*> tmpNodeOpt = node;

    while (tmpNodeOpt.has_value()) {
        auto tmpNode = tmpNodeOpt.value();

        // Always increase the denominator
        tmpNode->state.visitCount++;

        // Increase the numerator if it's winning
        if (status == BoardStatus::Win) {
            tmpNode->state.winScore += 4;
        }

        if (status == BoardStatus::Draw) {
            tmpNode->state.winScore += 1;
        }

        // Go up one level
        tmpNodeOpt = tmpNode->parent;
    }
}

std::pair<int, int> PlayerAgent::query() {
    const auto start_time = std::chrono::system_clock::now();
    Tree tree{new Node{State{localBoard, Cell::Opponent, lastMove}}};

    size_t loopCount = 0;
    do {
        loopCount++;
        // Select path based on UCT
        Node* selectedNode = selectNode(tree.root);

        // Create children (expand) for the node
        if (selectedNode->state.board.getStatus() == BoardStatus::Wip ||
            selectedNode == tree.root) {
            expand(selectedNode);
            if (selectedNode->children.size() == 0) {
                // logfile << "selectedNode has no children\n";
            }
        }

        // Choose node to simulate from
        Node* exploredNode = nullptr;
        if (selectedNode->children.size() > 0) {
            exploredNode = selectedNode->getRandChild();
        } else {
            exploredNode = selectedNode;
        }

        // Simulate
        const auto simulationStatus = simulateRandomly(exploredNode);

        // Propagate
        propagateBack(exploredNode, simulationStatus);
    } while (std::chrono::system_clock::now() - start_time <
             std::chrono::milliseconds(800));

    logfile << "loopCount: " << loopCount << "\n";
    logfile.flush();

    auto finalNodeOpt = tree.root->getMaxChild();
    if (finalNodeOpt.has_value()) {
        auto moveOpt = finalNodeOpt.value()->state.move;
        if (moveOpt.has_value()) {
            logfile << static_cast<double>(
                           finalNodeOpt.value()->state.winScore) /
                           static_cast<double>(
                               finalNodeOpt.value()->state.visitCount)
                    << "\n";
            logfile.flush();
            localBoard.performMove(Cell::Self, moveOpt.value());
            return moveOpt.value();
        } else {
            throw std::runtime_error("Move is nullopt");
        }
    } else {
        throw std::runtime_error("Final node is nullopt");
    }
}

void PlayerAgent::updateOpponentMove(std::pair<int, int> move) {
    this->lastMove = move;
    this->localBoard.performMove(Cell::Opponent, move);
}

void PlayerAgent::openLogFile(std::string path) { this->logfile.open(path); }

void PlayerAgent::reset() {
    localBoard = Board{};
    lastMove   = std::nullopt;
}

} // namespace mcts

void AI::init(bool order) {
    agent.reset();
    agent.openLogFile((order) ? ("mcts-o.txt") : ("mcts-x.txt"));
}

void AI::callbackReportEnemy(int x, int y) { agent.updateOpponentMove({x, y}); }

std::pair<int, int> AI::queryWhereToPut([[maybe_unused]] TA::UltraBoard ub) {
    return agent.query();
}

struct Dummy {
    ~Dummy() {
        while (mcts::deinitCount != 0) {
            std::this_thread::yield();
        }
    }
};

Dummy dummy{};
