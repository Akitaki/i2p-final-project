#ifndef MCTS_AI_H
#define MCTS_AI_H

#include "./support_types.h"

#include <TA/Wrapper/ai.h>
#include <TA/ultra_board.h>

#include <fstream>

namespace mcts {

/**
 * @brief Number of trees that are still being recycled. See
 * `mcts::Tree::~Tree()` for more information.
 */
extern std::atomic_size_t deinitCount;

class PlayerAgent {
  public:
    static void propagateBack(Node* node, BoardStatus status);

    static Node* selectNode(Node* root);

    static void expand(mcts::Node* node);

    static BoardStatus simulateRandomly(Node* node);

    std::pair<int, int> query();

    void updateOpponentMove(std::pair<int, int> move);

    void openLogFile(std::string path);

    void reset();

  private:
    std::optional<std::pair<int, int>> lastMove{};
    Board localBoard{};
    std::ofstream logfile{};
};
} // namespace mcts

class AI : public AIInterface {
  public:
    /**
     * @brief Get our side and store it in `selfIsO`
     */
    void init(bool order) override;

    /**
     * @brief Get enemy's move and store it in `mcts::lastMove`
     */
    void callbackReportEnemy(int x, int y) override;

    /**
     * @brief Query a move
     */
    std::pair<int, int> queryWhereToPut([[maybe_unused]] TA::UltraBoard ub) override;

  private:
    mcts::PlayerAgent agent;
};

#endif /* ifndef MCTS_AI_H */
