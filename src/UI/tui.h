#ifndef TUI_H
#define TUI_H

#include "../Game/game.h"
#include "./panels.h"

#include <atomic>
#include <cmath>

#ifdef __unix__
#include <sys/ioctl.h>
#include <unistd.h>
#else
#error "Currently the UI only support Unix terminals"
#endif

namespace TA {
class UltraBoard;
}

/** @brief Text user interface utilities */
namespace ui {

wchar_t tagToWchar(TA::BoardInterface::Tag tag);

const wchar_t* tagToColor(TA::BoardInterface::Tag tag);

class Wsize {
  public:
    /** @brief Update the window size value by calling `ioctl()` */
    inline void update() { ioctl(STDOUT_FILENO, TIOCGWINSZ, &wsize); }
    /** @brief Get the row count */
    inline size_t rows() const { return wsize.ws_row; }
    /** @brief Get the column count */
    inline size_t cols() const { return wsize.ws_col; }

  private:
    struct winsize wsize;
};

/* Functions to calculate panel dimensions */

inline PanelDim boardPanelDim(Wsize wsize) {
    return PanelDim(1, 1, wsize.rows(), std::floor(wsize.cols() * 0.6));
}

inline PanelDim logPanelDim(Wsize wsize) {
    const size_t boardPanelWidth = std::floor(wsize.cols() * 0.6);
    return PanelDim(
        1,
        boardPanelWidth + 1,
        std::floor(wsize.rows() * 0.6),
        wsize.cols() - boardPanelWidth);
}

inline PanelDim ctrlPanelDim(Wsize wsize) {
    const size_t boardPanelWidth = std::floor(wsize.cols() * 0.6);
    const size_t logPanelHeight  = std::floor(wsize.rows() * 0.6);
    return PanelDim(
        logPanelHeight + 1,
        boardPanelWidth + 1,
        wsize.rows() - std::floor(wsize.rows() * 0.6),
        wsize.cols() - boardPanelWidth);
}

class TUI final {
  public:
    TUI();

    ~TUI();

    /** @brief Start the UI on current thread */
    void start();

    void update(bool didWinSizeChange = false);

    void update(TA::UltraBoard ub);

  private:
    void startGame(
        std::optional<std::string> libpathA,
        std::optional<std::string> libpathB);

    /**
     * Terminal window size
     */
    Wsize wsize;

    util::Receiver<ui::CallbackEvent> events;
    util::Sender<ui::CallbackEvent> sender;

    /* The panels */
    BoardPanel boardPanel;

    LogPanel logPanel;

    CtrlPanel ctrlPanel;

    std::atomic<bool> isGameRunning = false;

    /** @brief Worker thread handles */
    std::vector<std::thread> workers;

    /** @brief Sender for sending control messages to game thread */
    std::optional<util::Sender<game::Game::CtrlMsg>> gameCtrlSender;

    enum class FocusIn { Ctrl, Board };

    FocusIn focusIn = FocusIn::Ctrl;
};
} // namespace ui

#endif
