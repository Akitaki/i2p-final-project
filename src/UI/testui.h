#ifndef TESTUI_H
#define TESTUI_H

namespace ui {
/**
 * @brief Minimal command line interface intended for testing player agents.
 */
struct TestUI {
    TestUI();
    void run();
};
} // namespace ui

#endif /* ifndef TESTUI_H */
