#ifndef RAND_UTIL
#define RAND_UTIL

#include <random>

namespace util {
/**
 * @brief Select a node randomly from the iterator range
 */
template <typename Iter, typename RandomGenerator>
Iter selectRandomly(Iter start, Iter end, RandomGenerator& g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

/**
 * @brief Select a node randomly from the iterator range
 */
template <typename Iter> Iter selectRandomly(Iter start, Iter end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return selectRandomly(start, end, gen);
}
} // namespace util

#endif /* ifndef RAND_UTIL */
